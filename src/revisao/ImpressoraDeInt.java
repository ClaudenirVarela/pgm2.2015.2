package revisao;


public class ImpressoraDeInt {
    
    /**
     * Exercício 1.
     * Escreva um método chamado imprimeEscadinha, que receba como parâmetro um 
     * número inteiro e imprima uma escadinha com os dígitos do mesmo.
     */
    public static void imprimeEscadinha(int valor) {
        while (valor > 0) {
            System.out.println(valor);
            valor = valor / 10;
        }
    }
    
    /**
     * Exercício 2.
     * Escreva um método chamado imprimeInvertido, que receba como parâmetro um 
     * número e imprima o mesmo em ordem invertida (de trás para a frente). 
     */
    public static void imprimeInvertido(int valor) {
        while (valor > 0) {
            System.out.print(valor % 10);
            valor = valor / 10;
        }
        System.out.println();
    }


    public static void main(String[] args) {
        imprimeEscadinha(12345);
        imprimeInvertido(12345);
    }
}

package aula;


public class Time {
    private byte horas, minutos, segundos;
        
    /**
     * Constrói um time referente à hora 00:00:00.
     */
    public Time() {
        setTime(0, 0, 0);
    }
    
    /**
     * Constrói um Time com valores especificados pelos parâmetros.
     * Usa o mecanismo definido no setTime() para definição do horário.
     * @param horas a quantidade de horas
     * @param minutos a quantidade de minutos
     * @param segundos a quantidade de segundos
     */
    public Time(int horas, int minutos, int segundos) {
        setTime(horas, minutos, segundos);
    }
    
    /**
     * Constrói um Time com o horário recebido como String.
     * Usa o mecanismo definido no setTime() para definição do horário.
     * @param time o horário completo em formato ("hh:mm:ss").
     */
    public Time(String time) {
        String[] valores = time.split(":");
        setTime(
                Integer.parseInt(valores[0]),
                Integer.parseInt(valores[1]),
                Integer.parseInt(valores[2])
        );
    }
    
    /**
     * Seta os valores de horas, minutos e segundos de acordo com os parâmetros.
     * O excedente em segundos é acrescentado em minutos e o excedente em
     * minutos é acrescentado em horas. Caso seja passado um valor para horas
     * que seja maior ou igual a 24, as horas são setadas como o excedente.
     *      Ex.: 25:61:304 vira 02:06:04
     * @param horas a quantidade de horas
     * @param minutos a quantidade de minutos
     * @param segundos a quantidade de segundo
     */
    public void setTime(int horas, int minutos, int segundos) {
        this.segundos = (byte) (segundos % 60);
        int totalMinutos = (segundos / 60 + minutos);
        this.minutos = (byte) (totalMinutos % 60);
        this.horas = (byte) ((totalMinutos / 60 + horas) % 24);
    }
    
    public void setTime(int segundos) {
        this.segundos = (byte) (segundos % 60);
        int totalMinutos = (segundos / 60 + minutos);
        this.minutos = (byte) (totalMinutos % 60);
        this.horas = (byte) ((totalMinutos / 60 + horas) % 24);
    }

    public byte getHoras() {
        return horas;
    }

    public byte getMinutos() {
        return minutos;
    }

    public byte getSegundos() {
        return segundos;
    }
    
    public void setHoras(int horas) {
        this.horas = (byte) horas;
    }
    
    public void setMinutos(int minutos) {
        this.minutos = (byte) minutos;
    }
    
    public void setSegundos(int segundos) {
        this.segundos = (byte) segundos;
    }
    
    public String toString() {
        return String.format("%02d:%02d:%02d", horas, minutos, segundos);
    }
}
